Here's a helpful resources list:

```
GameMaker subreddit, and our parent community
http://www.reddit.com/r/gamemaker/

Official forums, with multiple help-orientated subforums
https://forum.yoyogames.com/

Long list of excellent tutorials and examples
https://www.reddit.com/r/gamemaker/comments/3lyoik/game_maker_handbook_resources_for_beginners_an/

Dozens of well made GML scripts and snippets
http://www.gmlscripts.com/

Community-led manual for GMS1
http://gamemaker.communitymanual.com/

Official reference manuals
http://docs.yoyogames.com/
http://docs2.yoyogames.com/

YoYoGames bug report form (YYG account required)
https://account.yoyogames.com/report-bug
```
