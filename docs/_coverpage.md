![logo](_media/bot.png)

<!-- Empty i tag to prevent duplicate id generation -->
# GameMakerBot <i></i>

> The bot used by the /r/GameMaker Discord server.

[Repo](https://bitbucket.org/christopherwk210/gm-bot)
[Dive In](#gamemakerbot)

![color](#081D00)
